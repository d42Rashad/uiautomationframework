const moment = require('moment');
const assert = require('assert');
const fs = require("fs");
const util = require("util");
const readFile = util.promisify(fs.readFile);
var dateFormat = require('dateformat');

sharedVariable = new Map();
sharedVariable2 = {};
module.exports = {

    loadPage: async function(url, waitInSeconds) {
        var timeout = (waitInSeconds) ? (waitInSeconds * 1000) : DEFAULT_TIMEOUT;
        await driver.get(url);
        await driver.wait(until.elementLocated(by.css('body')), timeout);
    },

    getDOWString: async function(ind) {
        switch(ind){
            case 0:
                return 'Sunday';
            case 1:
                return 'Monday';                
            case 2:
                return 'Tuesday';                
            case 3:
                return 'Wednesday';                
            case 4:
                return 'Thursday';                
            case 5:
                return 'Friday';                
            case 6:
                return 'Saturday';                
        }
        return '';
    },    

    mapToObj: async function(m) {
      return Array.from(m).reduce((obj, [key, value]) => {
        obj[key] = value;
        return obj;
      }, {});        
    },    

    parseTimePart: async function(value) {
         if(value.includes('am')) {
            value = value.replace('am', 'a.m.');
            return value;
         }
         if(value.includes('pm')) {
            value = value.replace('pm', 'p.m.');
            return value;
         }             
         return null;
    },

    validateDateBetween: async function(value, text) {
        await helpers.log(true, `validateDateBetween: value: ${value} text: ${text}`);
        dateFormatString = 'MMMM D, YYYY, h:mm a';
        dateFormatString2 = 'YYYY-MM-DD HH:mm:ss.SSSSSS';
        dateFormatString3 = 'M/D/YY h:mm:ss a';
        upperBound = moment(value, [dateFormatString, dateFormatString2, dateFormatString3]).add(global.dateBufferMins, 'minutes');
        lowerBound = moment(value, [dateFormatString, dateFormatString2, dateFormatString3]).subtract(global.dateBufferMins, 'minutes');
        actualValue = moment(text, [dateFormatString, dateFormatString2, dateFormatString3]);
        expectedValue = moment(value, [dateFormatString, dateFormatString2, dateFormatString3]);
        console.log('lowerBound: ' + lowerBound.format(dateFormatString));
        console.log('upperBound: ' + upperBound.format(dateFormatString));
        console.log('actualValue: ' + actualValue.format(dateFormatString));
        console.log('expectedValue: ' + expectedValue.format(dateFormatString));
        var passed = false;
        if(moment(actualValue).isBetween(lowerBound, upperBound) ||
           moment(actualValue).isSame(lowerBound) || 
           moment(actualValue).isSame(upperBound)){
            passed = true;
        } else {
            passed = false;
        }
        console.log(`passed: ${passed}`);
        return passed;
    },

    addMinutes: async function(date, minutes) {
        return new Date(date.getTime() + minutes*60000);
    },

    subMinutes: async function(date, minutes) {
        return new Date(date.getTime() - minutes*60000);
    },    

    getGetElementText: function (element) {
        // get the element from the page
        return element.getText();
    },    

    getAttributeValueByXpath: async function (by, attributeName) {
        element = await driver.findElement(by);
        return element.getAttribute(attributeName);
    },

    waitUntilTitleEquals: function(expectedTitle, waitInMilliseconds) {
        var timeout = waitInMilliseconds || DEFAULT_TIMEOUT;
        return driver.wait(function() {
            return driver.getTitle().then(function(pageTitle) {
                return pageTitle === expectedTitle;
            });

        }, timeout, "textValue: " + expectedTitle);        
    },    

    substringBetween: async function(s, a, b) {
        console.log('STRING: ' + s);
        var p = s.indexOf(a) + a.length;
        return s.substring(p, s.indexOf(b, p));
    },

    waitUntilTextEquals: function(element, textValue, waitInMilliseconds) {
        var timeout = waitInMilliseconds || DEFAULT_TIMEOUT;
        return driver.wait(function() {
            return helpers.getGetElementText(element).then(function(value) {
                // console.log("value: " + value);
                return value === textValue;
            });

        }, timeout, "textValue: " + textValue);
    },

    waitForElementLocated: async function(by) {
        await driver.wait(until.elementLocated(by), 5 * 1000);
    },

    waitForWebElementLocated: async function(element) {
        await driver.wait(element, 5 * 1000);
    },    

    type: async function(by, text, variable) {
        await helpers.waitForElementLocated(by);
        await driver.wait(until.elementLocated(by), 5 * 1000);
        if (typeof variable === 'string') {
            if (variable == 'false') {
                await driver.findElement(by).sendKeys(text);
            }else {
                await driver.findElement(by).sendKeys(helpers.variables2[text]);
            }
        }
    },

    clear: async function(by) {
        await helpers.waitForElementLocated(by);
        await driver.wait(until.elementLocated(by), 5 * 1000);
        await driver.findElement(by).clear();
    },    

    typeElement: async function(element, text) {
        await helpers.scrollElementIntoView(element);
        await element.sendKeys(text);
    },    
    
    click: async function(by) {
        await driver.wait(until.elementLocated(by), 5 * 1000);
        await helpers.scrollIntoView(by);
        await driver.findElement(by).click();
    },

    noScrollClick: async function(by) {
        await driver.findElement(by).click();
    },    

    scrollIntoView: async function(by) {
        await driver.wait(until.elementLocated(by), 5 * 1000);
        await driver.executeScript("arguments[0].scrollIntoView();", helpers.getElement(by));
    },

    highlight: async function(element) {
        await driver.executeScript("arguments[0].style.border='3px solid red'", element);
    },    

    highlightElement: async function(element, color) {
        await driver.executeScript("arguments[0].style.border='3px solid " + color + "'", element);
    },     

    scrollElementIntoView: async function(element) {
        await driver.executeScript("arguments[0].scrollIntoView();", element);
    },    

    scrollElementIntoBottomView: async function(element) {
        await driver.executeScript("arguments[0].scrollIntoView(false);", element);
    },     

    getElement: async function(by) {
        await helpers.waitForElementLocated(by);
        return driver.findElement(by);
    },

    getElements: async function(by) {
        await helpers.waitForElementLocated(by);
        return driver.findElements(by);
    },    

    waitForElementNotLocated: async function(elementBy) {
        waitInMilliseconds = 5 * 1000;
        var timeout = waitInMilliseconds || DEFAULT_TIMEOUT;
        return driver.wait(function() {
            return helpers.getElement(elementBy).then(function(element) {
                return element.isDisplayed() == false;
            });

        }, timeout);
    },    

    moveTo: function(by) {
        return driver.wait(until.elementLocated(by), 5 * 1000).then(element => {
            return driver.actions({bridge: true})
            .move({x: 0, y: 0, origin: element})
            .perform(); 
        })
    },

    hover: async function(element) {
        await driver.actions({bridge: true}).move({x: 5, y: 5, origin: element}).perform();
        return element;
    },

    sleep: function(seconds){
        return new Promise(resolve => setTimeout(resolve, seconds * 1000));
    },

    select: async function(byElement, option){
        element = await helpers.getElement(byElement);
        option = await element.findElement(by.xpath('option[text() = \'' + option + '\']'));
        await element.click();
        await option.click();
    },

    elementSelect: async function(element, option){
        option = await element.findElement(by.xpath('option[text() = \'' + option + '\']'));
        await element.click();
        await option.click();
    },    

    pages: function() {
        hashMap = new Map();
        hashMap.set('switch', page.switch);
        hashMap.set('vserver', page.vserver);
        hashMap.set('shared', page.shared);
        hashMap.set('discoveryscores', page.discoveryscores);
        hashMap.set('physicaldevices', page.physicaldevices);
        hashMap.set('impactgraph', page.impactgraph);
        hashMap.set('cloud', page.cloud);
        hashMap.set('unknowndevices', page.unknowndevices);
        hashMap.set('appcomp', page.appcomp);
        hashMap.set('IPMI', page.IPMI);
        hashMap.set('configurationresource', page.configurationresource);
        hashMap.set('TCP', page.TCP);
        return hashMap;
    },

    getResultTable: async function(tableColumnsBy, tableBodyRowBy) {
        console.log(`getResultTable: tableColumnsBy: ${tableColumnsBy} tableBodyRowBy: ${tableBodyRowBy}`);
        childrenBy = by.xpath('child::*');
        bodyRows = await driver.findElements(tableBodyRowBy);
        console.log(`bodyRows.length: ${bodyRows.length}`);
        if(bodyRows != null && bodyRows.length > 0) {
            columnRow = await driver.findElements(tableColumnsBy);
            columnHeaders = await columnRow[1].findElements(childrenBy);
            let tableArray = [];
            for (i = 0; i < bodyRows.length; i++) {
                row = bodyRows[i];
                rowElements = await row.findElements(childrenBy);
                hashMap = new Map();
                console.log(`rowElements.length: ${rowElements.length}`);
                for (a = 0; a < rowElements.length; a++) {
                    rowElement = rowElements[a];
                    await helpers.scrollElementIntoView(rowElement);
                    columnRows = await columnHeaders[a].findElements(childrenBy);
                    column = columnRows[0];
                    columnSpan = await column.findElement(by.xpath('*'));
                    columnText = await columnSpan.getText();
                    await helpers.highlight(row);
                    hashMap.set(columnText, rowElement);
                }
                tableArray[i] = hashMap;
            }
            return tableArray;
        }
        return null;
    },    

    getResultTable2: async function(tableColumnsBy, tableBodyRowBy) {
        helpers.log(true, `getResultTable2: tableColumnsBy: ${tableColumnsBy} tableBodyRowBy: ${tableBodyRowBy}`);
        childrenBy = by.xpath('child::*');
        bodyRows = await driver.findElements(tableBodyRowBy);
        tableArray = [];
        console.log(`bodyRows.length: ${bodyRows.length}`);
        if(bodyRows != null && bodyRows.length > 0) {
            columnRow = await driver.findElement(tableColumnsBy);
            columnHeaders = await columnRow.findElements(childrenBy);
            console.log(`tableColumnsBy: ${tableColumnsBy}`);
            console.log(`tableColumnsBy: ${tableColumnsBy}`);
            console.log(`columnHeaders.length: ${columnHeaders.length}`)
            // for (i = 0; i < bodyRows.length; i++) {
            for (i = 0; i < 1; i++) {
                row = bodyRows[i];
                rowElements = await row.findElements(childrenBy);
                // hashMap = new Map();
                hashMap = {};
                console.log(`rowElements.length: ${rowElements.length}`);
                for (a = 0; a < rowElements.length-1; a++) {
                    rowElement = rowElements[a+1];
                    columnElement = await columnHeaders[a].getText();
                    await helpers.scrollElementIntoView(rowElement);
                    text = await rowElement.getText();
                    console.log(`columnElement: ${columnElement}`);
                    console.log(`text: ${text}`);
                    // await hashMap.set(columnElement, rowElement);
                    hashMap[columnElement] = rowElement;
                    // console.log(hashMap);
                    console.log('-----------------------------------');
                }
                tableArray[i] = hashMap;
            }
            console.log(tableArray);
            return tableArray;
        }
        return null;
    },    


    getColumnHeaders: async function(tableColumnsBy) {
        childrenBy = by.xpath('child::*');
        columnRow = await driver.findElements(tableColumnsBy);
        columnHeaders = await columnRow[1].findElements(childrenBy);
        return columnHeaders;
    },         

    getResultTable3: async function(tableColumnsBy, tableBodyRowBy, emptyColumns=['Select All', 'Run Now']) {
        helpers.log(true, `getResultTable3: tableColumnsBy: ${tableColumnsBy} tableBodyRowBy: ${tableBodyRowBy}`);
        childrenBy = by.xpath('child::*');
        bodyRows = await driver.findElements(tableBodyRowBy);
        console.log(`bodyRows.length: ${bodyRows.length}`);
        columnRow = await driver.findElement(tableColumnsBy);
        columnHeaders = await columnRow.findElements(childrenBy);
        console.log(`tableColumnsBy: ${tableColumnsBy}`);
        console.log(`tableColumnsBy: ${tableColumnsBy}`);
        console.log(`columnHeaders.length: ${columnHeaders.length}`)
        let tableArray = [];
        // for (i = 0; i < bodyRows.length; i++) {
        for (i = 0; i < bodyRows.length; i++) {
            row = bodyRows[i];
            rowElements = await row.findElements(childrenBy);
            hashMap = {};
            console.log(`rowElements.length: ${rowElements.length}`);
            for (a = 0; a < rowElements.length; a++) {
                rowElement = await rowElements[a];
                columnElement = await columnHeaders[a];
                columnText = await columnElement.getText();
                if(columnText == ''){
                    columnText = emptyColumns[0];
                    emptyColumns.shift();
                }
                console.log(`columnText: ${await columnText}`);
                await helpers.highlightElement(rowElement, 'red');
                // console.log(`rowElement: ${await rowElement.getText()}`);
                hashMap[columnText] = rowElement;
                console.log('-----------------------------------');
            }
            tableArray[i] = hashMap;
            emptyColumns=['Select All', 'Run Now'];
        }
        return tableArray;
    },       

    getResultTable4: async function(tableColumnsBy, tableBodyRowBy, emptyColumns=['Select All', 'Run Now']) {
        childrenBy = by.xpath('child::*');
        cellChildren = by.xpath('td[not(contains(@class, \'original\'))]');
        bodyRows = await driver.findElements(tableBodyRowBy);
        console.log(`bodyRows.length: ${bodyRows.length}`);
        columnRow = await driver.findElement(tableColumnsBy);
        columnHeaders = await columnRow.findElements(childrenBy);
        console.log(`tableColumnsBy: ${tableColumnsBy}`);
        console.log(`tableColumnsBy: ${tableColumnsBy}`);
        console.log(`columnHeaders.length: ${columnHeaders.length}`)
        let tableArray = [];
        // for (i = 0; i < bodyRows.length; i++) {
        for (i = 0; i < 1; i++) {
            row = bodyRows[i];
            rowElements = await row.findElements(cellChildren);
            hashMap = new Map();
            console.log(`rowElements.length: ${rowElements.length}`);
            for (a = 0; a < rowElements.length; a++) {
                rowElement = await rowElements[a];
                columnElement = await columnHeaders[a];
                columnText = await columnElement.getText();
                if(columnText == ''){
                    columnText = emptyColumns[0];
                    emptyColumns.shift();
                }
                console.log(`columnText: ${await columnText}`);
                // console.log(`rowElement: ${await rowElement.getText()}`);
                await hashMap.set(columnText, rowElement);
                console.log('-----------------------------------');
            }
            tableArray[i] = hashMap;
            emptyColumns=['Select All', 'Run Now']
        }
        return tableArray;
    },     

    getResultTableRow: async function(filterColumn, value, retries) {
        searchReuslt = false;
        tableColumnsBy = page.shared.elements().get('table_resultListColumns');
        tableBodyRowBy = page.shared.elements().get('table_resultListRows');
        childrenBy = by.xpath('child::*');
        bodyRows = await driver.findElements(tableBodyRowBy);
        if(bodyRows != null && bodyRows.length > 0) {
            columnRow = await driver.findElements(tableColumnsBy);
            columnHeaders = await columnRow[1].findElements(childrenBy);
            for (i = 0; i < bodyRows.length; i++) {
                row = bodyRows[i];
                rowElements = await row.findElements(childrenBy);
                for (a = 0; a < rowElements.length; a++) {
                    rowElement = rowElements[a];
                    await helpers.scrollElementIntoView(rowElement);
                    rowElementText = await rowElement.getText();
                    columnRows = await columnHeaders[a].findElements(childrenBy);
                    column = columnRows[0];
                    columnSpan = await column.findElement(by.xpath('*'));
                    columnText = await columnSpan.getText();
                    if(columnText == filterColumn && value == rowElementText) {
                        await helpers.highlight(row);
                        return row;
                    }
                }
            }
        }
        if(retries > 0) {
            await driver.navigate().refresh();
            returnValue = helpers.getResultTableRow(filterColumn, value, retries--);
            return returnValue;
        }
        return null;
    },

    getResultTableMap: async function(row) {
        tableColumnsBy = page.shared.elements().get('table_resultListColumns');
        childrenBy = by.xpath('child::*');
        columnRow = await driver.findElements(tableColumnsBy);
        columnHeaders = await columnRow[1].findElements(childrenBy);
        rowElements = await row.findElements(childrenBy);
        returnMap = new Map();
        for (a = 0; a < rowElements.length; a++) {
            rowElement = rowElements[a];
            await helpers.scrollElementIntoView(rowElement);
            columnRows = await columnHeaders[a].findElements(childrenBy);
            column = columnRows[0];
            columnSpan = await column.findElement(by.xpath('*'));
            columnText = await columnSpan.getText();
            await helpers.highlight(rowElement);
            returnMap.set(columnText, rowElement);
        }        
        return returnMap;
    },

    getJson: async (path) => {
        helpers.log(true, `getJson path: ${path}`);
        try {
            message = JSON.parse(
              await readFile(path)
            );
            await helpers.log(false, `json: ${message}`);
            return message;
        } catch (err) {
            throw err;
        }        
    },

    getVariable: async (key) => {
        console.log(`getVariable: key: ${key}`);        
        if(key == 'date') {
            var now = await new Date();
            returnDate = await dateFormat(now, "mmmm d, yyyy");
            return returnDate;
        }
        if(key.includes('icon')) {
            elementBy =  helpers.pages().get('shared').elements().get(key);
            return await helpers.getElement(element);
        }
        console.log(`key: ${key}`);
        console.log(`helpers.variables2: ${helpers.variables2}`);
        return await helpers.variables2[key];
    },  

    getResourceMap: async function(file,dataMap) {
        helpers.log(true, `getResourceMap file: ${file} dataMap: ${dataMap}`);
        data = await helpers.getJson('./resources/' + file + '.json');
        data = data[dataMap];
        return data;
    },    

    getPageElementDef: async function(page, elementName) {
        helpers.log(true, `getPageElementDef page: ${page} elementName: ${elementName}`);
        return helpers.pages().get(page).elements().get(elementName);
    },

    getPageElementBy_Replace: async function(page, elementName, replace) {
        helpers.log(true, `getPageElementDef page: ${page} elementName: ${elementName} replace: ${replace}`);
        elementString = await helpers.getPageElementDef(page,elementName);
        elementString = elementString.replace('$', replace);
        return await by.xpath(elementString);
    },

    log: async(markFunction,message) => {
        if(markFunction)
            message = `FUNCION: ${message}`
        else
            message = `-     ${message}`
        console.log(message);
    },

    validateDate: async(element,expectedValue) => {
        actualValue = await element.getText();
        upperBound = moment(expectedValue, 'MMMM D, YYYY, h:mm a')
                            .add(global.dateBufferMins, 'minutes')
                            .format('MMMM D, YYYY, h:mm a');
        lowerBound = moment(expectedValue, 'MMMM D, YYYY, h:mm a')
                            .subtract(global.dateBufferMins, 'minutes')
                            .format('MMMM D, YYYY, h:mm a');  
        upperBound = await helpers.parseTimePart(upperBound);
        lowerBound = await helpers.parseTimePart(lowerBound);
        var passed = false;
        if(expectedValue.trim()==actualValue.trim() || actualValue.trim()==upperBound.trim() || actualValue.trim()==lowerBound.trim()) {
            passed = true;
        }
        assert(passed==true, `actualValue: ${actualValue} expectedValue: ${expectedValue}`);
    },    

    validateCell: async(contains, element, expectedValue, imgBool=null) => {
        await helpers.log(true, `validateCell: contains: ${contains} element: ${element} expectedValue: ${expectedValue} imgBool: ${imgBool}`);
        // await helpers.highlightElement(element, "yellow");
        switch(contains) {
            case 'true':
                actualValue = await element.getText();
                await assert(actualValue.includes(expectedValue), `actualValue: ${actualValue} expectedValue: ${expectedValue}`);  
                break;
            case 'false':
                actualValue = await element.getText();
                await helpers.log(false,`actualValue: ${actualValue}`);
                await assert(actualValue==expectedValue, `actualValue: ${actualValue} expectedValue: ${expectedValue}`);                
                break;
            case 'date':
                actualValue = await element.getText();
                await helpers.highlightElement(element, 'yellow');
                console.log(`actualValue: ${actualValue}`);
                console.log(`expectedValue: ${expectedValue}`);
                passed = await helpers.validateDateBetween(expectedValue,actualValue);
                assert(await passed==true, `actualValue: ${actualValue} | expectedValue: ${expectedValue}`);                 
                break;                
            case 'obj':
                assert(await imgBool.isDisplayed()==true, "Validating if object is displayed.");                 
                break;     
            case 'gt':
                actualValue = await element.getText();
                actualValue = await parseInt(actualValue, 10);
                expectedValue = await parseInt(expectedValue, 10);
                assert(actualValue>expectedValue, `actualValue: ${actualValue} expectedValue: ${expectedValue}`);
                break;
            case 'lt':
                actualValue = await element.getText();
                actualValue = await parseInt(actualValue, 10);
                expectedValue = await parseInt(expectedValue, 10);
                assert(actualValue<expectedValue, `actualValue: ${actualValue} expectedValue: ${expectedValue}`);
                break;                                
        }  
    },

    processCell: async(key, tableRow, item, page, additionalLayer, action='validate') => {
        await helpers.log(true, `processCell: key: ${key} tableRow: ${JSON.stringify(tableRow)} item: ${item} page: ${page} additionalLayer: ${additionalLayer}`);
        console.log(await Object.keys(tableRow));        
        if(key != 'validateVariable' && key != 'contains') {
            element = tableRow[key];
            expectedValue = item[key];
            obj = null;
            console.log(`---->>>>>>expectedValue: ${expectedValue}`);
            if(item['validateVariable'] == 'true') {
                expectedValue = await helpers.getVariable(item[key]);
            }
            if(item['validateVariable'] == 'obj') {
                subElementBy = await helpers.pages().get(page).elements().get(expectedValue);
                if(additionalLayer!=null) {
                    tempElement = await element.findElement(by.xpath(additionalLayer));
                    obj = await tempElement.findElement(subElementBy);
                } else {
                    obj = await element.findElement(subElementBy);                    
                }
            }
            switch(action) {
                case 'validate':
                    await helpers.validateCell(item['contains'], element, expectedValue, obj);
                    break;
                case 'click':
                    console.log(`element.getTagName(): ${await element.getTagName()}`);
                    break;                    
            }
        }
    },   
    variables: sharedVariable,
    variables2: {}
};
