'use strict';

var firefox = require('geckodriver');
var selenium = require('selenium-webdriver');

/**
 * Creates a Selenium WebDriver using Firefox as the browser
 * @returns {ThenableWebDriver} selenium web driver
 */

const capabilities = {
    browserName: 'firefox',
    javascriptEnabled: true,
    acceptSslCerts: true,
    acceptInsecureCerts: true,
    'webdriver.firefox.bin': firefox.path
}

module.exports = function() {

    var driver = new selenium.Builder()
    .usingServer(process.env.HUB_IP)
    .withCapabilities(capabilities).build();

    driver.manage().window().maximize();

    return driver;
};
