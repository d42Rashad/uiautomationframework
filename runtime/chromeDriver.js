'use strict';

var chromedriver = require('chromedriver');
var selenium = require('selenium-webdriver');

/**
 * Creates a Selenium WebDriver using Chrome as the browser
 * @returns {ThenableWebDriver} selenium web driver
 */

const capabilities = {
    browserName: 'chrome',
    javascriptEnabled: true,
    acceptSslCerts: true,
    acceptInsecureCerts: true,
    chromeOptions: {
        args: ['start-maximized', 'disable-extensions']
    },
    path: chromedriver.path
}

module.exports = function() {

    var driver = new selenium.Builder()
    .usingServer(process.env.HUB_IP)
    .withCapabilities(capabilities).build();

    driver.manage().window().maximize();

    return driver;
};
