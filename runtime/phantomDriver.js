'use strict';

var phantomjs = require('phantomjs-prebuilt');
var selenium = require('selenium-webdriver');

/**
 * Creates a Selenium WebDriver using PhantomJS as the browser
 * @returns {ThenableWebDriver} selenium web driver
 */

const capabilities = {
    browserName: 'phantomjs',
    javascriptEnabled: true,
    acceptSslCerts: true,
    'phantomjs.binary.path': phantomjs.path,
    'phantomjs.cli.args': '--ignore-ssl-errors=true'
}

module.exports = function() {

    var driver = new selenium.Builder()
    .usingServer(process.env.HUB_IP)
    .withCapabilities(capabilities).build();

    driver.manage().window().maximize();

    return driver;
};
