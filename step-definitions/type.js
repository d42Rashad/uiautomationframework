const moment = require('moment');
const fs = require('fs');
const util = require('util');
module.exports = function () {

    this.Then(/^Type: text: "([^"]*)" obj: "([^"]*)" page: "([^"]*)" variable: "([^"]*)"$/, 
        async function (text, obj, page, variable) {

        await helpers.type(helpers.pages().get(page).elements().get(obj), text, variable);
    });

    this.Then(/^Type field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" variable: "([^"]*)" value: "([^"]*)"$/, 
    async function (obj, page, column, variable, value) {
        console.log(`Type field-box text: obj: ${obj} page: ${page} column: ${column} variable: ${variable} value: ${value}`);
        elementString = await helpers.pages().get(page).elements().get(obj);
        elementBy = by.xpath(elementString.replace('$',column));
        await helpers.type(elementBy, value, variable);
    });

    this.Then(/^Type field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" value: "([^"]*)"$/, 
    async function (obj, page, column, value) {
        console.log(`Type field-box text: obj: ${obj} page: ${page} column: ${column} value: ${value}`);
        elementString = await helpers.pages().get(page).elements().get(obj);
        elementBy = by.xpath(elementString.replace('$',column));
        element = await helpers.getElement(elementBy);
        await element.sendKeys(value);
    });

};
