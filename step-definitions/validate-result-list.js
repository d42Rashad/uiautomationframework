const moment = require('moment');
module.exports = function () {

    this.Then(/^Validate Result List: searchColumn: "([^"]*)" searchValue: "([^"]*)" validateColumn: "([^"]*)" validateValue: "([^"]*)" searchVariable: "([^"]*)" validateVariable: "([^"]*)"$/, 
    async function (searchColumn, searchValue, validateColumn, validateValue, searchVariable, validateVariable) {
        if (searchVariable == 'true')
            // searchValue = helpers.variables.get(searchValue);
            searchValue = helpers.variables2[searchValue];
        if (validateVariable == 'true')
            // validateValue = helpers.variables.get(validateValue);
            validateValue = helpers.variables2[validateValue];
        tableRow = await helpers.getResultTableRow(searchColumn, searchValue,1);
        rowMap = await helpers.getResultTableMap(tableRow);
        await helpers.waitUntilTextEquals(rowMap.get(validateColumn), validateValue, 120000);
    }); 

    this.Then(/^Validate Result List: searchColumn: "([^"]*)" searchValue: "([^"]*)" validateColumn: "([^"]*)" validateValue: "([^"]*)" searchVariable: "([^"]*)" validateVariable: "([^"]*)" timeout: "([^"]*)"$/, 
    async function (searchColumn, searchValue, validateColumn, validateValue, searchVariable, validateVariable, timeout) {
        if (searchVariable == 'true')
            // searchValue = helpers.variables.get(searchValue);
            searchValue = helpers.variables2[searchValue];
        if (validateVariable == 'true')
            // validateValue = helpers.variables.get(validateValue);
            validateValue = helpers.variables2[validateValue];
        tableRow = await helpers.getResultTableRow(searchColumn, searchValue,1);
        rowMap = await helpers.getResultTableMap(tableRow);
        var timeoutInt = await parseInt(timeout, 10);
        await helpers.waitUntilTextEquals(rowMap.get(validateColumn), validateValue, timeoutInt * 1000);
    }); 

    this.Then(/^Validate Result List: searchColumn: "([^"]*)" searchValue: "([^"]*)" validateColumn: "([^"]*)" validateValue: "([^"]*)" searchVariable: "([^"]*)" validateVariable: "([^"]*)" contains: "([^"]*)"$/, 
    async function (searchColumn, searchValue, validateColumn, validateValue, searchVariable, validateVariable, contains) {
        if (searchVariable == 'true')
            // searchValue = helpers.variables.get(searchValue);
            searchValue = helpers.variables2[searchValue];
        if (validateVariable == 'true')
            // validateValue = helpers.variables.get(validateValue);
            validateValue = helpers.variables2[validateValue];
        tableRow = await helpers.getResultTableRow(searchColumn, searchValue,1);
        rowMap = await helpers.getResultTableMap(tableRow);
        actualValue = await rowMap.get(validateColumn);
        actualValue = await actualValue.getText();
        expectedValue = validateValue;
        switch(contains) {
            case 'true':
                assert(actualValue.includes(expectedValue), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);  
                break;
            case 'false':
                assert(expectedValue.trim()==actualValue.trim(), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);
                break;
            case 'date':
                passed = await helpers.validateDateBetween(expectedValue,actualValue);
                assert(passed==true, `expectedValue: ${expectedValue} actualValue: ${actualValue}`);
                break;
        }
    });           

    this.Then(/^Validate Result List Not Displayed: searchColumn: "([^"]*)" searchValue: "([^"]*)" searchVariable: "([^"]*)"$/, 
    async function (searchColumn, searchValue, searchVariable) {
        if (searchVariable == 'true')
            // searchValue = helpers.variables.get(searchValue);
            searchValue = helpers.variables2[searchValue];
        tableRow = await helpers.getResultTableRow(searchColumn, searchValue,0);
        assert(tableRow==null, "Ensuring row doesn't exist."); 
    }); 


    this.Then(/^Validate Result List: file: "([^"]*)" dataMap: "([^"]*)"$/, 
    async function (fileName, dataMap) {
        console.log(`Validate Result List: file: ${fileName} dataMap: ${dataMap}`);
        data = await helpers.getJson('./resources/' + fileName + '.json');
        data = data[dataMap];
        console.log(`data: ${data}`);
        tableColumnsBy = page.shared.elements().get('table_resultListColumns');
        tableBodyRowBy = page.shared.elements().get('table_resultListRows');
        tableArray = await helpers.getResultTable(tableColumnsBy, tableBodyRowBy);
        //loop over ui table rows
        console.log(`tableArray.length: ${tableArray.length}`);
        for (tr = 0; tr < tableArray.length;  tr++) {
            tableRow = tableArray[tr]; //this is the current table row
            console.log(`tableRow: ${tableRow}`);
            console.log(`data.length: ${data.length}`);
            for (er = 0; er < data.length;  er++) { //loop over all json rows
                expRow = data[er]; //this is the current json row
                searchVariable = expRow.searchVariable;
                searchColumn = expRow.searchColumn;
                searchValue = expRow.searchValue;
                console.log(`expRow: ${searchVariable}`);
                // console.log(`searchVariable: ${searchVariable}`);
                console.log(`searchColumn: ${searchColumn}`);
                // console.log(`searchValue: ${searchValue}`);
                console.log(`searchVariable: ${searchVariable}`);
                if (searchVariable == 'true')
                    // searchValue = helpers.variables.get(searchValue);
                    searchValue = helpers.variables2[searchValue];
                console.log(`searchValue: ${searchValue}`);
                currentTableColumn = tableRow.get(expRow.searchColumn);
                columnText = await currentTableColumn.getText();
                // console.log('searchValue: ' + searchValue);
                console.log('columnText: ' + columnText);
                console.log('expRow.searchValue: ' + expRow.searchValue);
                if(columnText == searchValue) {
                    for (rv = 0; rv < expRow.rowValues.length;  rv++) {
                        item = await expRow.rowValues[rv];
                        console.log('item: ' + JSON.stringify(item));
                        for (var key in item) {
                            if (item.hasOwnProperty(key)) {
                                if(key != 'validateVariable' && key != 'contains') {
                                    console.log(`key: ${key}`);
                                    element = tableRow.get(key);
                                    expectedValue = await item[key];
                                    if(item['validateVariable'] == 'true') {
                                        expectedValue = await helpers.getVariable(item[key]);
                                    }
                                    console.log(`expectedValue: ${expectedValue}`);                                    
                                    if(item['validateVariable'] == 'obj') {
                                        await helpers.highlightElement(element, 'yellow');
                                        imgBool = await element.findElement(await helpers.pages().get('shared').elements().get(item[key]));
                                        isDisplayed = await imgBool.isDisplayed();
                                        assert(isDisplayed==true, "Validating if object is displayed.");                           
                                    }                        
                                    switch(item['contains']) {
                                        case 'true':
                                            actualValue = await element.getText();
                                            assert(actualValue.includes(expectedValue), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);  
                                            break;
                                        case 'false':
                                            actualValue = await element.getText();
                                            assert(await expectedValue.trim()== await actualValue.trim(), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);
                                            break;
                                        case 'date':
                                            actualValue = await element.getText();
                                            passed = await helpers.validateDateBetween(expectedValue,actualValue);
                                            assert(passed==true, "Validating date!");
                                            break;
                                        case 'gt':
                                            actualValue = await element.getText();
                                            actualValue = await parseInt(actualValue, 10);
                                            expectedValue = await parseInt(expectedValue, 10);
                                            assert(actualValue>expectedValue, `actualValue: ${actualValue} expectedValue: ${expectedValue}`);
                                            break;
                                        case 'lt':
                                            actualValue = await element.getText();
                                            actualValue = await parseInt(actualValue, 10);
                                            expectedValue = await parseInt(expectedValue, 10);
                                            assert(actualValue<expectedValue, `actualValue: ${actualValue} expectedValue: ${expectedValue}`);
                                            break;                                            
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        console.log(`--------------------`);
    });      

    this.Then(/^Validate Result List: page: "([^"]*)" replace: "([^"]*)" file: "([^"]*)" dataMap: "([^"]*)"$/, 
    async function (page, replace, file, dataMap) {
        helpers.log(true, `Validate Result List page: ${page} replace: ${replace} file: ${file} dataMap: ${dataMap}`);
        data = await helpers.getResourceMap(file,dataMap);
        tableColumnsBy = await helpers.getPageElementBy_Replace(page,'tableColumns', replace);
        tableBodyRowBy = await helpers.getPageElementBy_Replace(page,'tableRows', replace);
        tableArray = await helpers.getResultTable2(tableColumnsBy, tableBodyRowBy);
        console.log(`tableArray.length: ${tableArray.length}`);
        console.log(`tableArray: ${await JSON.stringify(tableArray)}`);        
        for (tr = 0; tr < tableArray.length;  tr++) {
            console.log(`data.length: ${data.length}`);
            for (er = 0; er < data.length;  er++) {
                searchValue = data[er].searchValue;
                contains = 'false';
                console.log(`searchValue: ${searchValue}`);
                rowValues = data[er].rowValues;
                console.log(`rowValues: ${await JSON.stringify(rowValues)}`);
                if (data[er].searchVariable == 'true')                       
                    searchValue = helpers.variables2[searchValue];
                if (data[er].contains == 'true')                       
                    contains = 'true';
                columnText = await tableArray[tr][data[er].searchColumn].getText();
                console.log(`columnText: ${columnText}`);
                if(columnText == searchValue) {
                    for (rv = 0; rv < rowValues.length;  rv++) {
                        item = rowValues[rv];
                        for (var key in item) {
                            console.log('!!!--------->>>>>>>>>>>>>' + await JSON.stringify(tableArray[tr]));
                            await helpers.processCell(key, tableArray[tr], item, 'shared', 'p');
                        }
                    }
                }
            }
        }
    });  
};
