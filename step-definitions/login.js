module.exports = function () {

    this.Given(/^I open Device42 admin and log in$/, async function () {
        await helpers.loadPage(process.env.APPURL);
        await page.login.loginAsAdmin();
    });
    
};
