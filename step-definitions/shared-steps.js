const moment = require('moment');
const fs = require('fs');
const util = require('util');
module.exports = function () {

    this.Then(/^Validate object text: obj: "([^"]*)" page: "([^"]*)"$/, 
    async function (obj, page) {
        element = await helpers.getElement(helpers.pages().get(page).elements().get(obj));
        elementText = await element.getText();
        expectedText = `Current time zone: ${process.env.ZIME_ZONE}`;
        console.log(`elementText: ${elementText}`);
        assert(elementText == expectedText, `Validating time zone: expected: ${expectedText} actual: ${elementText}`);
    });    

    this.Given(/^Set Variable: text: "([^"]*)" value: "([^"]*)" timestamp: "([^"]*)"$/, async function (text, value, timestamp) {
        if(timestamp == 'true') {
            value = value + new Date().getTime();
        }
        testMap = await helpers.variables2;
        testMap[text] = await value;
        console.log(JSON.stringify(await helpers.variables2));
    });

    this.Given(/^Select Regions$/, async function () {
        await page.cloud.addRegions(['ap-northeast-1', 'ap-northeast-2', 'ap-south-1', 'ap-southeast-1',
                                     'ap-southeast-2', 'ca-central-1', 'eu-central-1', 'eu-west-1', 'eu-west-2',
                                     'sa-east-1', 'us-east-1', 'us-east-2', 'us-west-1', 'us-west-2']);
    });

    this.Given(/^Select Zones "([^"]*)"$/, async function (zones) {
        stringArray = await zones.split(",");
        await page.cloud.addZones(stringArray);
    });
    this.Given(/^Validate Application Component Detail file: "([^"]*)" serviceMap: "([^"]*)" propertiesMap: "([^"]*)"$/, async function (file, serviceMap, propertiesMap) {
        detailsServices = await helpers.getElements(helpers.pages().get('appcomp').elements().get('detailsServices'));
        detailsProperties = await helpers.getElements(helpers.pages().get('appcomp').elements().get('detailsProperties'));

        // data = await helpers.getJson('./resources/' + 't572TestData' + '.json');
        data = await helpers.getJson('./resources/' + file + '.json');
        console.log(`data: ${data}`);
        // services = await data['jboss-90920Services'];
        // properties = await data['jboss-90920Properties'];

        services = await data[serviceMap];
        properties = await data[propertiesMap];  

        console.log(JSON.stringify(services));      

        for (ds = 0; ds < detailsServices.length;  ds++) {
            await helpers.highlightElement(detailsServices[ds], 'red');

            rows = await detailsServices[ds].findElements(by.xpath('table/tbody/tr'));
            actualMap = {};

            for (r = 0; r < rows.length;  r++) {
                rowKey = await rows[r].findElement(by.xpath('th'));
                rowValue = await rows[r].findElement(by.xpath('td/span'));
                await helpers.highlightElement(rows[r], 'red');
                await helpers.highlightElement(rowKey, 'green');
                await helpers.highlightElement(rowValue, 'green');

                rowKey = await rowKey.getText();
                rowValue = await rowValue.getText();

                console.log(`rowKey: ${rowKey}`);
                console.log(`rowValue: ${rowValue}`);  

                actualMap[rowKey] = rowValue;
            }
            console.log(`actualMap: ${await JSON.stringify(actualMap)}`); 
            serviceValidated = false;
            for (s = 0; s < services.length;  s++) {
                currentServiceRow = await services[s];
                console.log(`currentServiceRow.service_display_name: ${currentServiceRow.service_display_name}`);
                console.log(`currentServiceRow.service_name: ${currentServiceRow.service_name}`);
                console.log(`currentServiceRow.user: ${currentServiceRow.user}`);
                console.log(`currentServiceRow.device_id: ${currentServiceRow.device_id}`);
                console.log(`=============`);
                console.log(`actualMap.service_display_name: ${actualMap.service_display_name}`);
                console.log(`actualMap.service_name: ${actualMap.service_name}`);
                console.log(`actualMap.user: ${actualMap.user}`);
                console.log(`actualMap.device_id: ${actualMap.device_id}`);
                console.log(`-----------------------------------------------------------`);
                if(currentServiceRow.service_display_name == actualMap.service_display_name &&
                   currentServiceRow.service_name == actualMap.service_name && 
                   currentServiceRow.user == actualMap.user && 
                   currentServiceRow.state == actualMap.state) {
                    serviceValidated = true;
                    break;
                }
            }
            console.log(`serviceValidated: ${serviceValidated}`);         
        }
        for (dp = 0; dp < detailsProperties.length;  dp++) {
            propertyRow = await detailsProperties[dp];
            header = await propertyRow.findElement(by.xpath('th'));
            data = await propertyRow.findElement(by.xpath('td/span'));
            await helpers.highlightElement(header, 'red');
            await helpers.highlightElement(data, 'red');
            headerText = await header.getText();
            dataText = await data.getText();
            console.log(`headerText: ${headerText}`);
            console.log(`dataText: ${dataText}`);
            console.log(`properties[headerText]: ${await JSON.stringify(properties[headerText])}`);
            assert(properties[headerText] == dataText, `Validating device details: expected: ${properties[headerText]} actual: ${dataText}`);
        }        

    });    

    this.Then(/^Click Result List: page: "([^"]*)" replace: "([^"]*)" searchColumn: "([^"]*)" searchValue: "([^"]*)" searchVariable: "([^"]*)" clickColumn: "([^"]*)"$/, 
    async function (page, replace, searchColumn, searchValue, searchVariable, clickColumn) {        
        helpers.log(true, `Validate Result List page: ${page} replace: ${replace}`);
        tableColumnsBy = await helpers.getPageElementBy_Replace(page,'tableColumns', replace);
        tableBodyRowBy = await helpers.getPageElementBy_Replace(page,'tableRows', replace);
        tableArray = await helpers.getResultTable3(tableColumnsBy, tableBodyRowBy);
        console.log(`tableArray.length: ${tableArray.length}`);
        for (tr = 0; tr < tableArray.length;  tr++) {
            console.log(`--->>>>>: ${await JSON.stringify(tableArray[tr][searchColumn])}`);
            cell = await tableArray[tr][searchColumn];
            children = await cell.findElements(by.xpath('child::*'));
            console.log(`cell.getTagName(): ${await cell.getTagName()}`);
            console.log(`children.length: ${await children.length}`);
            console.log(`children[0].getTagName(): ${await children[0].getTagName()}`);
            await helpers.highlightElement(children[0], 'red');
            actualTableText = await children[0].getText();
            if(actualTableText == searchValue) {
                await children[0].click();
                break;
            }
        }
    });  

    this.Then(/^Click Result List: page: "([^"]*)" replace: "([^"]*)" searchColumn: "([^"]*)" searchValue: "([^"]*)" searchVariable: "([^"]*)" clickColumn: "([^"]*)" contains: "([^"]*)"$/, 
    async function (page, replace, searchColumn, searchValue, searchVariable, clickColumn, contains) {        
        helpers.log(true, `Validate Result List page: ${page} replace: ${replace}`);
        tableColumnsBy = await helpers.getPageElementBy_Replace(page,'tableColumns', replace);
        tableBodyRowBy = await helpers.getPageElementBy_Replace(page,'tableRows', replace);
        tableArray = await helpers.getResultTable3(tableColumnsBy, tableBodyRowBy);
        console.log(`tableArray.length: ${tableArray.length}`);
        for (tr = 0; tr < tableArray.length;  tr++) {
            console.log(`--->>>>>: ${await JSON.stringify(tableArray[tr][searchColumn])}`);
            cell = await tableArray[tr][searchColumn];
            children = await cell.findElements(by.xpath('child::*'));
            console.log(`cell.getTagName(): ${await cell.getTagName()}`);
            console.log(`children.length: ${await children.length}`);
            console.log(`children[0].getTagName(): ${await children[0].getTagName()}`);
            await helpers.highlightElement(children[0], 'red');
            actualTableText = await children[0].getText();
            if(actualTableText.includes(searchValue) && contains == 'true') {
                await children[0].click();
                break;
            }
        }
    });       


    this.Then(/^Click Result List: searchColumn: "([^"]*)" searchColumnValue: "([^"]*)" searchVariable: "([^"]*)" clickColumn: "([^"]*)"$/, 
    async function (searchColumn, searchColumnValue, searchVariable, clickColumn) {
        if (searchVariable == 'true')    
            searchColumnValue = helpers.variables2[searchColumnValue];  
        tableColumnsBy = await helpers.pages().get('shared').elements().get('table_resultListColumns');
        tableBodyRowBy = await helpers.pages().get('shared').elements().get('table_resultListRows');
        tableArray = await helpers.getResultTable3(tableColumnsBy, tableBodyRowBy);
        console.log(`tableArray.length: ${tableArray.length}`);
        for (tr = 0; tr < tableArray.length;  tr++) {
            row = tableArray[tr];
            matchText = await row[searchColumn].getText();
            element = row[clickColumn];
            console.log(`searchColumn: ${searchColumn}`);
            console.log(`matchText: ${matchText}`);
            console.log(`element.getTagName(): ${await element.getTagName()}`);
            children = await element.findElements(by.xpath('child::*'));
            console.log(`children.length: ${await children.length}`);
            console.log(`searchColumnValue: ${await searchColumnValue}`);
            if(searchColumnValue == matchText) {
                if(children.length > 0) {
                    await helpers.highlightElement(children[0], 'green');
                    await helpers.scrollElementIntoBottomView(children[0]);
                    await children[0].click();
                    break;
                } else {
                    await helpers.highlightElement(element, 'green');
                    await helpers.scrollElementIntoBottomView(element);
                    await element.click();
                    break;
                }
            }
        }
    });

    // this.Given(/^Select all in table$/, async function () {
    //     tableColumnsBy = by.xpath("//table[@id='result_list']/thead/tr");
    //     columns = await helpers.getColumnHeaders(tableColumnsBy);
    //     console.log(`columns.length: ${columns.length}`);
    //     console.log(`columns[0]: ${JSON.stringify(columns[0])}`);
    //     console.log(`columns[0].getTagName(): ${await columns[0].getTagName()}`);
    //     element = await columns[0].findElement(by.xpath("div/span/input"));
    //     await element.click();
    // });               


    this.Given(/^Save test variables map to file: "([^"]*)"$/, async function (fileName) {
        // v = await helpers.variables;
        // console.log(`variable: ${v}`);
        var mapString = JSON.stringify(await helpers.variables2);
        console.log(`mapString: ${mapString}`);
        fs.writeFile(`${process.env.PERSISTEDTESTDATAFOLDER}/${fileName}`, mapString, (err) => {
            if (err) throw err;
            console.log('Map saved!');
        });        
    });    

    this.Given(/^Load test variables map to file: "([^"]*)"$/, async function (fileName) {   
        const readFile = util.promisify(fs.readFile); 
        var data = await fs.readFileSync(`${process.env.PERSISTEDTESTDATAFOLDER}/${fileName}`, 
            {encoding:'utf8', flag:'r'}); 
        var r = await JSON.parse(data);
        helpers.variables2 = await r;
    }); 

    this.Given(/^Set Timestamp Variable: text: "([^"]*)"$/, async function (text) {
        if (text.includes('Full')){
            value = await moment().utcOffset('+00:00').format('YYYY-MM-DD HH:mm:ss.SSSSSS');
             if(value.includes('am')) {
                value = value.replace('am', 'a.m.');
                }
             if(value.includes('pm')) {
                value = value.replace('pm', 'p.m.');
                }         
         helpers.variables2[text] = value;
         console.log(helpers.variables2);
        }
        else if (text.includes('Debug')){
            value = await moment().utcOffset('-04:00').format('M/D/YY h:mm:ss a');
            if(value.includes('am')) {
                value = value.replace('am', 'AM');
                }
             if(value.includes('pm')) {
                value = value.replace('pm', 'PM');
                }         
         helpers.variables2[text] = value;
         console.log(helpers.variables2);
        }
        else {
            value = await moment().utcOffset('+00:00').format('MMMM D, YYYY, h:mm a');
            if(value.includes('am')) {
                value = value.replace('am', 'a.m.');
                }
             if(value.includes('pm')) {
                value = value.replace('pm', 'p.m.');
                }         
         helpers.variables2[text] = value;
         console.log(helpers.variables2);
        }
    });

    this.Then(/^Navigate to: "([^"]*)"$/, async function (path) {
        await helpers.loadPage(process.env.APPURL + path);
    });

    this.Then(/^Delete all available$/, async function () {

        tableColumnsBy = by.xpath("//table[@id='result_list']/thead/tr");
        tableBodyRowBy = by.xpath("//table[@id='result_list']/tbody/tr");
        bodyRows = await driver.findElements(tableBodyRowBy);
        if(bodyRows.length > 0) {

            columns = await helpers.getColumnHeaders(tableColumnsBy);
            console.log(`columns.length: ${columns.length}`);
            console.log(`columns[0]: ${JSON.stringify(columns[0])}`);
            console.log(`columns[0].getTagName(): ${await columns[0].getTagName()}`);
            console.log(`bodyRows.length: ${bodyRows.length}`);
                    
            element = await columns[0].findElement(by.xpath("div/span/input"));
            await element.click();
            await helpers.select(helpers.pages().get('shared').elements().get('select_action'), 'Delete with Detailed Confirmation');
            runSelAction = await helpers.getElement(helpers.pages().get('shared').elements().get('button_RunSelAction'));
            await runSelAction.click();
            await helpers.waitUntilTitleEquals('Are you sure? | Device42', 60000);
            yesImSure = await helpers.getElement(helpers.pages().get('shared').elements().get('button_YesImSure'));
            await yesImSure.click();
            await helpers.waitUntilTitleEquals('Select Device to view | Device42', 60000);
        }
    });

    this.Then(/^Click: obj: "([^"]*)" page: "([^"]*)"$/, async function (obj, page) {
        await helpers.click(helpers.pages().get(page).elements().get(obj));
    });

    this.Then(/^Validate isDisplayed: obj: "([^"]*)" page: "([^"]*)"$/, async function (obj, page) {
        console.log(`Validate isDisplayed: obj: ${obj} page: ${page}`);
        elementBy = helpers.pages().get(page).elements().get(obj);
        // console.log(`elementString: ${elementString}`);
        element = await helpers.getElement(elementBy);        
        isDisplayed = await element.isDisplayed();
        assert(isDisplayed==true, `Validating if object (${obj}) on page (${page}) is displayed.`);  
    });    

 //    this.Then(/^Type: text: "([^"]*)" obj: "([^"]*)" page: "([^"]*)" variable: "([^"]*)"$/, 
 //        async function (text, obj, page, variable) {

 //    	await helpers.type(helpers.pages().get(page).elements().get(obj), text, variable);
	// });

    this.Then(/^Select field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" variable: "([^"]*)" value: "([^"]*)"$/, 
    async function (obj, page, column, variable, value) {
        console.log(`Select field-box text: obj: ${obj} page: ${page} column: ${column} variable: ${variable} value: ${value}`);
        if (variable == 'true')
            value = helpers.variables2[value];        
        elementString = await helpers.pages().get(page).elements().get(obj);
        elementBy = by.xpath(elementString.replace('$',column));
        await helpers.scrollIntoView(elementBy);
        await helpers.select(elementBy, value);
    });    

    // this.Then(/^Select field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" variable: "([^"]*)" value: "([^"]*)"$/, 
    // async function (obj, page, column, variable, value) {
    //     console.log(`>>Validate field-box text: obj: ${obj} page: ${page} column: ${column} variable: ${variable} value: ${value}`);
    //     if (variable == 'true')
    //         value = await helpers.getVariable(value);    
    //         console.log(`value(updated): ${value}`);
    //     elementString = await helpers.pages().get(page).elements().get(obj);
    //     elementBy = by.xpath(elementString.replace('$',column));

    //     await helpers.select(elementBy, value);
    // });      

    this.Then(/^Select: option: "([^"]*)" obj: "([^"]*)" page: "([^"]*)"$/, async function (option, obj, page) {
        await helpers.select(helpers.pages().get(page).elements().get(obj), option);
    });        

    this.Then(/^Select: option: "([^"]*)" obj: "([^"]*)" page: "([^"]*)" variable: "([^"]*)" replace: "([^"]*)"$/, 
        async function (option, obj, page, variable, replace) {
            if (variable == 'true')
                // option = helpers.variables.get(option);
                option = helpers.variables2[option];
            elementString = helpers.pages().get(page).elements().get(obj);
            elementString = helpers.pages().get(page).elements().get(obj);
            elementString = elementString.replace('$', replace);
            elementBy = await by.xpath(elementString);
            await helpers.select(elementBy, option);
    });  

    this.Then(/^Click: obj: "([^"]*)" page: "([^"]*)" replace: "([^"]*)"$/, 
        async function (obj, page, replace) {
            elementString = helpers.pages().get(page).elements().get(obj);
            elementString = elementString.replace('$', replace);
            elementBy = await by.xpath(elementString);
            element = await helpers.getElement(elementBy);
            await helpers.scrollElementIntoView(element);
            await element.click();
    });      

    this.Then(/^Check: obj: "([^"]*)" page: "([^"]*)"$/, async function (obj, page) {
    	checkbox = helpers.pages().get(page).elements().get(obj);
    	result = await helpers.getAttributeValueByXpath(checkbox,'checked');
    	if(!result) {
    		await helpers.click(checkbox);
    	}
	});

    this.Then(/^Uncheck: obj: "([^"]*)" page: "([^"]*)"$/, async function (obj, page) {
    	checkbox = helpers.pages().get(page).elements().get(obj);
    	result = await helpers.getAttributeValueByXpath(checkbox,'checked');
    	if(result) {
    		await helpers.click(checkbox);
    	}
	});

    this.Then(/^Click Result List "([^"]*)": column: "([^"]*)" value: "([^"]*)" variable: "([^"]*)"$/, 
    async function (actionColumn, filterColumn, value, variable) {
        if (variable == 'true')
            // value = helpers.variables.get(value);
            value = helpers.variables2[value];
        tableRow = await helpers.getResultTableRow(filterColumn, value,0);
        rowMap = await helpers.getResultTableMap(tableRow);
        element = rowMap.get(actionColumn);
        await helpers.highlightElement(element, 'green');
        console.log(`element.getTagName(): ${await element.getTagName()}`);

        children = await element.findElements(by.xpath('child::*'));
        console.log(`children.length: ${children.length}`);

        if(children.length > 0) {
            // await helpers.highlightElement(children[0], 'red');
            await children[1].click();
        } else {
            await element.click();
        }
    });      

    this.Then(/^Wait for title to equal: "([^"]*)"$/, 
    async function (pageTitle){
        await helpers.waitUntilTitleEquals(pageTitle, 60000);
    });

    this.Then(/^Wait for title to equal: "([^"]*)" replace: "([^"]*)" variable: "([^"]*)"$/, 
    async function (pageTitle, replace, variable){
        if (variable == 'true')
            // replace = helpers.variables.get(replace);
            replace = helpers.variables2[replace];
        pageTitle = pageTitle.replace('$', replace);  
        await helpers.waitUntilTitleEquals(pageTitle, 60000);
    });    

    this.Then(/^Wait in seconds: "([^"]*)"$/, 
    async function (sec){
        await helpers.sleep(parseInt(sec, 10));
    });    

    this.Then(/^Validate table-field-box: column: "([^"]*)" value: "([^"]*)"$/, 
    async function (column, value) {
        jobStatusContainerBy = helpers.pages().get('switch').elements().get('div_JobStatus');
        element = await helpers.getElement(jobStatusContainerBy);
        await helpers.scrollElementIntoView(element);
        jobStatusRows = await element.findElements(by.xpath('table/tbody/tr'));
        for (i = 0; i < jobStatusRows.length; i++) {
            rowChildren = await jobStatusRows[i].findElements(by.xpath('child::*'));
            columnName = await rowChildren[0].getText();
            rowValue = await rowChildren[1].getText();
            await helpers.scrollElementIntoView(rowChildren[0]);
            await helpers.highlight(rowChildren[0]);
            if(column == columnName) {
                assert(value==rowValue, "Validating text in table field-box.");
                break;
            }
        }
    });     

    this.Then(/^Switch to window with title: "([^"]*)" value: "([^"]*)" variable: "([^"]*)" replace: "([^"]*)"$/, 
    async function (title, value, variable, replace) {
        if (variable == 'true')
            // value = helpers.variables.get(value);
            value = helpers.variables2[value];
        if (replace == 'true')
            title = title.replace('$', value);    
        windows = await driver.getAllWindowHandles();
        length = await windows.length;
        found = false;
        for (i = 0; i < windows.length; i++) {
            await driver.switchTo().window(windows[i]);
            windowTitle = await driver.getTitle();
            if(title == windowTitle) {
                found = true;
                break;
            }            
        }
    });

    this.Then(/^Switch to main window$/, 
    async function (title, value, variable, replace) {
        if (variable == 'true')
            // value = helpers.variables.get(value);
            value = helpers.variables2[value];
        if (replace == 'true')
            title = title.replace('$', value);    
        windows = await driver.getAllWindowHandles();
        length = await windows.length;
        found = false;
        await driver.switchTo().window(windows[0]);
    });

    // this.Then(/^Type field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" value: "([^"]*)"$/, 
    // async function (obj, page, column, value) {
    //     console.log(`>>Validate field-box text: obj: ${obj} page: ${page} column: ${column} value: ${value}`);
    //     elementString = await helpers.pages().get(page).elements().get(obj);
    //     elementBy = by.xpath(elementString.replace('$',column));
    //     element = await helpers.getElement(elementBy);
    //     await element.sendKeys(value);
    // });    

    this.Then(/^Click field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)"$/, 
    async function (obj, page, column) {
        console.log(`>>Validate field-box text: obj: ${obj} page: ${page} column: ${column}`);
        elementString = await helpers.pages().get(page).elements().get(obj);
        elementBy = by.xpath(elementString.replace('$',column));
        element = await helpers.getElement(elementBy);
        await helpers.scrollElementIntoView(element);
        await helpers.highlight(element);
        await element.click();
    });        

    // this.Then(/^Select field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" value: "([^"]*)"$/, 
    // async function (obj, page, column, value) {
    //     console.log(`>>Validate field-box text: obj: ${obj} page: ${page} column: ${column} value: ${value}`);
    //     elementString = await helpers.pages().get(page).elements().get(obj);
    //     elementBy = by.xpath(elementString.replace('$',column));
    //     element = await helpers.getElement(elementBy);
    //     await helpers.elementSelect(element,value);
    // });    

    this.Then(/^Validate field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" value: "([^"]*)" variable: "([^"]*)" contains: "([^"]*)"$/, 
    async function (obj, page, column, value, variable, contains) {
        console.log(`>>Validate field-box text: obj: ${obj} page: ${page} column: ${column} value: ${value} variable: ${variable}  contains: ${contains}`);
        if (variable == 'true')
            value = await helpers.getVariable(value);    
            console.log(`value(updated): ${value}`);
        elementString = await helpers.pages().get(page).elements().get(obj);
        elementBy = by.xpath(elementString.replace('$',column));

        elements = await helpers.getElements(elementBy);
        element = await elements[elements.length-1];

        await helpers.highlightElement(element, 'red');
        console.log(`elementString: ${elementString}`);
        console.log(`column: ${column}`);
        if (value.includes('icon_')) {
            await helpers.highlightElement(element, 'yellow');
            imgBool = await element.findElement(await helpers.pages().get('shared').elements().get(value));
            isDisplayed = await imgBool.isDisplayed();
            assert(isDisplayed==true, "Validating if object is present.");                   
        } else {
            text = await element.getText();
            console.log(`text: ${text}`);
            switch(contains) {
                case 'true':
                    assert(text.includes(value), `column: ${column} expected: ${value} actual: ${text} Validating text in field-box.`);       
                    break;
                case 'false':
                    assert(value==text, `column: ${column} expected: ${value} actual: ${text} Validating text in field-box.`);
                    break;
                case 'date':
                    passed = await helpers.validateDateBetween(value,text);
                    assert(passed==true, "Validating date!");
                    break;
            }             
        }
    });

    // this.Then(/^Type field-box text: obj: "([^"]*)" page: "([^"]*)" column: "([^"]*)" variable: "([^"]*)" value: "([^"]*)"$/, 
    // async function (obj, page, column, variable, value) {
    //     console.log(`>>Validate field-box text: obj: ${obj} page: ${page} column: ${column} variable: ${variable} value: ${value}`);
    //     elementString = await helpers.pages().get(page).elements().get(obj);
    //     elementBy = by.xpath(elementString.replace('$',column));
    //     await helpers.type(elementBy, value, variable);
    // });         

    this.Then(/^Validate table field box: file: "([^"]*)" dataMap: "([^"]*)"$/, 
    async function (fileName, dataMap) {
        console.log(`Validate table field box: file: ${fileName} dataMap: ${dataMap}`);
        data = await helpers.getJson('./resources/' + fileName + '.json');
        console.log(`data: ${data}`);
        data = data[dataMap];
        console.log(`data(updated): ${data}`);
        jobStatusContainerBy = helpers.pages().get('switch').elements().get('div_JobStatus');
        element = await helpers.getElement(jobStatusContainerBy);
        await helpers.scrollElementIntoView(element);
        jobStatusRows = await element.findElements(by.xpath('table/tbody/tr'));
        console.log(`jobStatusContainerBy: ${jobStatusContainerBy}`);
        console.log(`jobStatusRows.length: ${jobStatusRows.length}`);
        for (i = 0; i < jobStatusRows.length; i++) {
            rowClass = await jobStatusRows[i].getAttribute('class');
            console.log(`rowClass: ${rowClass}`);
            if(rowClass != 'job-status-item start-section') {
                rowChildren = await jobStatusRows[i].findElements(by.xpath('child::*'));
                columnName = await rowChildren[0].getText();
                rowValue = await rowChildren[1].getText();
                console.log(`i: ${i}`);
                console.log(`columnName: ${columnName}`);
                await helpers.scrollElementIntoView(rowChildren[0]);
                await helpers.highlight(rowChildren[0]);
                console.log(`data.length: ${data.length}`);
                for (x = 0; x < data.length;  x++) {
                    item = await data[x];
                    objecKeys = await Object.keys(item);
                    for (y = 0; y < objecKeys.length;  y++) {
                        key = await objecKeys[y];
                        if(columnName == key) {
                            validateVariable = item['validateVariable'];
                            expectedValue = item[key];
                            if(validateVariable == 'true') {
                                expectedValue = await helpers.getVariable(expectedValue);
                            }
                            if(key != 'validateVariable' && key != 'contains' && columnName == key) {
                                contains = item['contains'];
                                actualValue = rowValue;
                                console.log('--------------------------------');   
                                console.log(`Found ${columnName}`);
                                console.log(`rowValue: ${rowValue}`);
                                // console.log(`x: ${x}`);
                                console.log(`key: ${key}`);
                                console.log(`expectedValue: ${expectedValue}`);
                                console.log(`validateVariable: ${validateVariable}`);
                                console.log(`contains: ${contains}`);
                                console.log('--------------------------------');     

                                switch(contains) {
                                    case 'true':
                                        assert(actualValue.includes(expectedValue), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);  
                                        break;
                                    case 'false':
                                        assert(expectedValue.trim()==actualValue.trim(), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);
                                        break;
                                    case 'date':
                                        passed = await helpers.validateDateBetween(expectedValue,actualValue);
                                        assert(passed==true, `expectedValue: ${expectedValue} actualValue: ${actualValue}`);
                                        break;
                                }                            

                            }
                        }                    
                    }
                }
            }            
        }           
    }); 

    this.Then(/^Validate field-set: page: "([^"]*)" replace: "([^"]*)" file: "([^"]*)" dataMap: "([^"]*)"$/, 
    async function (page, replace, file, dataMap) {
        data = await helpers.getJson('./resources/' + file + '.json');
        data = data[dataMap];

        tableColumnString = helpers.pages().get(page).elements().get('tableColumns');
        tableRowString = helpers.pages().get(page).elements().get('tableRows');

        tableColumnString = tableColumnString.replace('$', replace);
        tableRowString = tableRowString.replace('$', replace);

        tableColumnBy = by.xpath(tableColumnString);
        tableRowBy = by.xpath(tableRowString);

        columns = await driver.findElements(tableColumnBy); 
        rows = await driver.findElements(tableRowBy);

        let tableArray = [];
        for (ri = 0; ri < rows.length;  ri++) {
            row = rows[ri];
            cells = await row.findElements(by.xpath('child::*'));
            hashMap = new Map();
            for (rc = 0; rc < cells.length;  rc++) {
                columnText = await columns[rc].getText();
                cell = cells[rc];
                rowText = await cell.getText();
                hashMap.set(columnText, rowText);
            }
            tableArray[ri] = hashMap;
        }
        for (tr = 0; tr < tableArray.length;  tr++) {
            tableRow = tableArray[tr];
            for (er = 0; er < data.length;  er++) {
                expRow = data[er];
                if(tableRow.get(expRow.searchColumn) == expRow.searchValue) {
                    for (rv = 0; rv < expRow.rowValues.length;  rv++) {
                        item = expRow.rowValues[rv];
                        for (var key in item) {
                            if (item.hasOwnProperty(key)) {
                                if(key != 'validateVariable' && key != 'contains') {
                                    expectedValue = item[key];
                                    actualValue = tableRow.get(key);
                                    assert(expectedValue==actualValue, "Validating text in table field-set table.");  
                                }
                            }
                        }
                    }
                }
            }
        }
    });    

    this.Then(/^xxValidate field-set: page: "([^"]*)" replace: "([^"]*)" file: "([^"]*)" dataMap: "([^"]*)"$/, 
    async function (page, replace, file, dataMap) {
        data = await helpers.getJson('./resources/' + file + '.json');
        data = data[dataMap];

        tableColumnString = helpers.pages().get(page).elements().get('tableColumns');
        tableRowString = helpers.pages().get(page).elements().get('tableRows');

        tableColumnString = tableColumnString.replace('$', replace);
        tableRowString = tableRowString.replace('$', replace);

        tableColumnBy = by.xpath(tableColumnString);
        tableRowBy = by.xpath(tableRowString);

        columns = await driver.findElements(tableColumnBy); 
        rows = await driver.findElements(tableRowBy);

        let tableArray = [];
        for (ri = 0; ri < rows.length;  ri++) {
            row = rows[ri];
            cells = await row.findElements(by.xpath('child::*'));
            hashMap = new Map();
            for (rc = 0; rc < cells.length;  rc++) {
                columnText = await columns[rc].getText();
                cell = cells[rc];
                rowText = await cell.getText();
                hashMap.set(columnText, rowText);
            }
            tableArray[ri] = hashMap;
        }
        for (tr = 0; tr < tableArray.length;  tr++) {
            tableRow = tableArray[tr];
            for (er = 0; er < data.length;  er++) {
                expRow = data[er];
                if(tableRow.get(expRow.searchColumn) == expRow.searchValue) {
                    for (rv = 0; rv < expRow.rowValues.length;  rv++) {
                        item = expRow.rowValues[rv];
                        for (var key in item) {
                            if (item.hasOwnProperty(key)) {
                                if(key != 'validateVariable' && key != 'contains') {
                                    element = tableRow.get(key);
                                    expectedValue = item[key];
                                    if(item['validateVariable'] == 'true') {
                                        expectedValue = await helpers.getVariable(item[key]);
                                    }
                                    if(item['validateVariable'] == 'obj') {
                                        await helpers.highlightElement(element, 'yellow');
                                        imgBool = await element.findElement(await helpers.pages().get('shared').elements().get(item[key]));
                                        isDisplayed = await imgBool.isDisplayed();
                                        assert(isDisplayed==true, "Validating if object is displayed.");                           
                                    }                        
                                    switch(item['contains']) {
                                        case 'true':
                                            actualValue = await element.getText();
                                            assert(actualValue.includes(expectedValue), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);  
                                            break;
                                        case 'false':
                                            actualValue = await element.getText();
                                            assert(await expectedValue.trim()== await actualValue.trim(), key + " - actualValue: " + actualValue + "\n" + "expectedValue: " + expectedValue);
                                            break;
                                        case 'date':
                                            actualValue = await element.getText();
                                            passed = await helpers.validateDateBetween(expectedValue,actualValue);
                                            assert(passed==true, "Validating date!");
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    });    

};
