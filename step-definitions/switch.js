const moment = require('moment');
module.exports = function () {

    this.Then(/^Click Auto Discovery Schedule Table: searchIndex: "([^"]*)" clickColumn: "([^"]*)" columnsElement: "([^"]*)" rowsElement: "([^"]*)"$/, 
    async function (searchIndex, clickColumn, columnsElement, rowsElement) {
        await page.switch.getTableCellElement(parseInt(searchIndex, 10), clickColumn, columnsElement, rowsElement);
        await cellElements[0].click();
    });      

    this.Then(/^Type Auto Discovery Schedule Table: searchIndex: "([^"]*)" clickColumn: "([^"]*)" columnsElement: "([^"]*)" rowsElement: "([^"]*)" text: "([^"]*)"$/, 
    async function (searchIndex, clickColumn, columnsElement, rowsElement, text) {
        await page.switch.getTableCellElement(parseInt(searchIndex, 10), clickColumn, columnsElement, rowsElement);
        await helpers.typeElement(cellElements[0], text);
    });  

    this.Then(/^Set Auto Discovery Schedule Table: index: "([^"]*)" dow: "([^"]*)" scheduledTime: "([^"]*)" columnsElement: "([^"]*)" rowsElement: "([^"]*)" variableName: "([^"]*)"$/, 
    async function (index, dow, scheduledTime, columnsElement, rowsElement, variableName) {
        console.log(`index: ${index} | dow: ${dow} | scheduledTime: ${scheduledTime} | columnsElement: ${columnsElement} | rowsElement: ${rowsElement} | variableName: ${variableName}`);
            var ad = new Date();
            var d = new Date(ad.toLocaleString('en-US', { timeZone: "UTC" }));
            var n = d.getDay();
            dow = await helpers.getDOWString(n+1);
            console.log(`dow: ${dow}`);
            // scheduledTime = `09:11:00`;
            scheduledTimeArray = await scheduledTime.split(':');
            hours = scheduledTimeArray[0];
            minutes = scheduledTimeArray[1];
            console.log(`scheduledTime: ${scheduledTime}`);
            tomorrow = moment().add(1, 'days');
            var date = moment(tomorrow).set({"hour": parseInt(hours, 10), "minute": parseInt(minutes, 10)});
            formattedScheduledTime = await date.format('MMMM D, YYYY, h:mm a');
            formattedScheduledTime = await helpers.parseTimePart(formattedScheduledTime);
            console.log(`Scheduled run date: ${formattedScheduledTime}`);
            helpers.variables2[variableName] = await formattedScheduledTime;

            row = await page.switch.getTableRowElement(parseInt(index, 10), columnsElement, rowsElement);
            for (i = 0; i < 7; i++) {
                currentDOW = await helpers.getDOWString(i);
                console.log(`currentDOW: ${currentDOW}`);
                console.log(`row: ${row}`);
                cell = await row.get(currentDOW);
                childrenBy = by.xpath('child::*');
                console.log(`cell: ${cell}`);
                cellElements = await cell.findElements(childrenBy);
                console.log(`cellElements.length: ${cellElements.length}`);
                if(currentDOW != dow) {
                    await cellElements[0].click();
                }

            }
            cell = await row.get('Time');
            childrenBy = by.xpath('child::*');
            console.log(`cell: ${cell}`);
            cellElements = await cell.findElements(childrenBy);
            console.log(`cellElements.length: ${cellElements.length}`);            
            await helpers.typeElement(cellElements[0], scheduledTime);
            console.log(`utcDate: ${d}`);
            console.log(`helpers.variables2: ${JSON.stringify(await helpers.variables2)}`);
    });  


};
