Feature: Discovery SNMP - Schedule

@t256a @scheduledJobSetup
Scenario: [D42-T256] Scheduled job with Local RC Scheduling
	Given Set Variable: text: "D42_T256_JobName" value: "SNMP-LocalRC-Scheduled" timestamp: "true"
	Given Set Variable: text: "D42_T256_JobServers" value: "10.42.255.3" timestamp: "false"
	Given I open Device42 admin and log in
	Then Navigate to: "/admin/autodiscover/switch/"
	Then Click: obj: "button_addSNMPAutoDiscoveryJob" page: "switch"
	Then Type: text: "D42_T256_JobName" obj: "input_jobName" page: "switch" variable: "true"
	Then Type: text: "D42_T256_JobServers" obj: "textarea_jobName" page: "switch" variable: "true"
	Then Check: obj: "checkbox_ICMP_TCP_PortCheck" page: "switch"
	Then Type: text: "117" obj: "textbox_CommunityStrings" page: "switch" variable: "false"
	Then Select: option: "v2c" obj: "select_SNMPVersion" page: "switch"	
	Given Set Timestamp Variable: text: "D42_T256_JobAdded"
	Then Click: obj: "button_Save" page: "shared"
	Then Wait for title to equal: "Select SNMP Autodiscovery Job to view | Device42"
	Then Type: text: "D42_T256_JobName" obj: "textbox_searchBar" page: "shared" variable: "true"
	Then Click: obj: "button_searchSubmit" page: "shared"
	Then Click Result List: searchColumn: "Job name" searchColumnValue: "D42_T256_JobName" searchVariable: "true" clickColumn: "Job name"
	Then Wait for title to equal: "View SNMP Autodiscovery Job - $ | Device42" replace: "D42_T256_JobName" variable: "true"
	Then Click: obj: "edit_button" page: "switch"
	Then Click: obj: "addADS_button" page: "switch"
	Then Set Auto Discovery Schedule Table: index: "0" dow: "today" scheduledTime: "09:11:00" columnsElement: "autoDiscoveryColumns" rowsElement: "autoDiscoveryRows" variableName: "D42_T256_JobStart"
	Then Click: obj: "button_Save" page: "shared"
	Then Save test variables map to file: "D42_T256_ParameterMap"

@t256b @scheduledJobValidation
Scenario: [D42-T256] Scheduled job with Local RC Validation
	Then Load test variables map to file: "D42_T256_ParameterMap"
	Given I open Device42 admin and log in
	Then Navigate to: "/admin/autodiscover/active_jobscore/"
	Then Wait for title to equal: "Jobs Dashboard | Jobs Summary | Device42"
	Then Validate Result List: searchColumn: "Job Name" searchValue: "D42_T256_JobName" validateColumn: "Detailed Discovery" validateValue: "100%" searchVariable: "true" validateVariable: "false" timeout: "3000"
	Then Validate Result List: searchColumn: "Job Name" searchValue: "D42_T256_JobName" validateColumn: "Detailed Queue Processing" validateValue: "100%" searchVariable: "true" validateVariable: "false" timeout: "3000"
	Given Set Timestamp Variable: text: "D42_T256_JobEnd"
	Then Validate Result List: file: "t256TestData" dataMap: "jobscoreTable"
	Then Click Result List: searchColumn: "Job Name" searchColumnValue: "D42_T256_JobName" searchVariable: "true" clickColumn: "Job Name"
	Then Switch to window with title: "View SNMP Autodiscovery Job - $ | Device42" value: "D42_T256_JobName" variable: "true" replace: "true"
	Then Validate field-box text: obj: "label" page: "switch" column: "Job name:" value: "D42_T256_JobName" variable: "true" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Remote Collector:" value: "(None)" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Server(s):" value: "10.42.255.3" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Exclude Server(s):" value: "" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Ignore OS(s):" value: "" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Port:" value: "161" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "SNMP version:" value: "v2c" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "ICMP/TCP Port Check:" value: "icon_yes" variable: "obj" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Strip domain name:" value: "icon_no" variable: "obj" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Give precedence to hostname:" value: "icon_no" variable: "obj" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Service Level:" value: "(None)" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Customer for discovered devices:" value: "(None)" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Object Category for discovered devices:" value: "(None)" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Overwrite existing object categories:" value: "icon_no" variable: "obj" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Tags for discovered devices:" value: "" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Job Debug level:" value: "Debug Off" variable: "false" contains: "false"
	Then Validate field-box text: obj: "label" page: "switch" column: "Start time of most recent job:" value: "D42_T256_JobStart" variable: "true" contains: "date"
	Then Validate table field box: file: "t256TestData" dataMap: "jobStatusTableFieldBox"
	Then Navigate to: "/admin/rackraj/device/"
	Then Type: text: "nh-lab-switch-01" obj: "textbox_searchBar" page: "shared" variable: "false"
	Then Click: obj: "button_searchSubmit" page: "shared"
	Then Validate Result List: file: "t256TestData" dataMap: "allDevicesTable"
	Then Navigate to: "/admin/autodiscover/discoveryscores/"
	Then Select: option: "D42_T256_JobName" obj: "select_Filter" page: "discoveryscores" variable: "true" replace: "By Job Name"
	Then Validate Result List: file: "t256TestData" dataMap: "discoveryScoresTable"	
	Then Wait in seconds: "300"
	Then Navigate to: "/admin/autodiscover/active_jobscore/"
	Then Wait for title to equal: "Jobs Dashboard | Jobs Summary | Device42"
	Then Validate Result List Not Displayed: searchColumn: "Job Name" searchValue: "D42_T256_JobName" searchVariable: "true"
	Then Navigate to: "/admin/autodiscover/completed_jobscore/"
	Then Validate Result List: file: "t256TestData" dataMap: "completedJobscoreTable"
	Then Wait in seconds: "300"
	Then Navigate to: "/admin/rackraj/device_unknown/"
	Then Wait for title to equal: "Select Unknown Device to view | Device42"
	Then Type: text: "nh-lab-switch-01" obj: "textbox_searchBar" page: "shared" variable: "false"
	Then Click: obj: "button_searchSubmit" page: "shared"
	Then Click Result List "Name": column: "Name" value: "nh-lab-switch-01" variable: "false"
	Then Wait for title to equal: "View Unknown Device - nh-lab-switch-01 | Device42"	
	Then Validate field-box text: obj: "label" page: "switch" column: "Network Device:" value: "icon_yes" variable: "obj" contains: "obj"	
	Then Validate field-box text: obj: "label" page: "switch" column: "Name:" value: "nh-lab-switch-01" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Type:" value: "unknown" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Blade Chassis:" value: "icon_no" variable: "obj" contains: "obj"	
	Then Validate field-box text: obj: "label" page: "switch" column: "Virtual/Container Host:" value: "icon_no" variable: "obj" contains: "obj"	
	Then Validate field-box text: obj: "label" page: "switch" column: "In Service:" value: "icon_yes" variable: "obj" contains: "obj"
	Then Validate field-box text: obj: "label" page: "switch" column: "Service Level:" value: "Production" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Is Device42 monitoring enabled:" value: "No" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Hardware:" value: "2924XLv, cisco" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Serial #:" value: "" variable: "false" contains: "true" 
	Then Validate field-box text: obj: "label" page: "switch" column: "UUID:" value: "" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Customer:" value: "(None)" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Group Permissions:" value: "None" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Do not propagate device permissions:" value: "icon_no" variable: "obj" contains: "obj"
	Then Validate field-box text: obj: "label" page: "switch" column: "Asset #:" value: "" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "First added:" value: "D42_T256_JobStart" variable: "true" contains: "date" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Last updated:" value: "D42_T256_JobEnd" variable: "true" contains: "date" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Tags:" value: "" variable: "false" contains: "false" 
	Then Validate field-box text: obj: "label" page: "switch" column: "Notes:" value: "" variable: "false" contains: "false" 
	Then Click: obj: "tabs" page: "physicaldevices" replace: "Other"
	Then Validate Result List: page: "unknowndevices" replace: "Discovery Scores" file: "t256TestData" dataMap: "UnknownDevice:DiscoveryScoresTable"
	Then Click: obj: "tabs" page: "physicaldevices" replace: "Properties"
	Then Validate Result List: page: "unknowndevices" replace: "Connectivity" file: "t256TestData" dataMap: "UnknownDevice:ConnectivityTable"
	Then Validate Result List: page: "unknowndevices" replace: "IP Address" file: "t256TestData" dataMap: "UnknownDevice:IPAddressTable"

