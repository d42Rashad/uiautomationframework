module.exports = {

    url: '',

    elements: function() {
        hashMap = new Map();
        hashMap.set('detailsServices', by.xpath('//div[@id=\'json\']/table/tbody/tr/th[text() = \'services\']/following-sibling::td/table/tbody/tr/td'));
        hashMap.set('detailsProperties', by.xpath('//div[@id=\'json\']/table/tbody/tr/th[text() = \'properties\']/following-sibling::td/table/tbody/tr'));
        return hashMap;
    }
    
};
