module.exports = {
	    elements: function() {
        hashMap = new Map();
        hashMap.set('button_Save', by.xpath('//input[@value=\'Save\']'));
        hashMap.set('table_resultList', by.xpath('//table[@id=\'result_list\']'));
        hashMap.set('textbox_searchBar', by.xpath('//input[@id=\'searchbar\']'));
        hashMap.set('button_searchSubmit', by.xpath('//input[@id=\'searchbar\']/following-sibling::button[@type=\'submit\']'));
        hashMap.set('table_resultListColumns', by.xpath("//table[@id='result_list']/thead/tr"));
        hashMap.set('table_resultListRows', by.xpath("//table[@id='result_list']/tbody/tr"));
        hashMap.set('icon_no', by.xpath("img[@src='/static/admin/img/icon-no.gif']"));
        hashMap.set('icon_yes', by.xpath("img[@src='/static/admin/img/icon-yes.gif']"));
        hashMap.set('fieldbox-text-input', '//label[text() = \'$\']/following-sibling::input');
        hashMap.set('fieldbox-text-select', '//label[text() = \'$\']/following-sibling::select');
        hashMap.set('fieldbox-text-textarea', '//label[text() = \'$\']/following-sibling::textarea');

        hashMap.set('label', '//label[text() = \'$\']/following-sibling::p');
        hashMap.set('input', '//label[text() = \'$\']/following-sibling::input');
        hashMap.set('select', '//label[text() = \'$\']/following-sibling::select');

        hashMap.set('select_action', by.xpath("//select[@name=\'action\']"));
        hashMap.set('button_RunSelAction', by.xpath('//button[@title=\'Run the selected action\']'));        
        hashMap.set('button_YesImSure', by.xpath("//input[@type='submit']"));

        hashMap.set('label_clock', by.xpath("//div[@class='clock']"));

        hashMap.set('time_zone', by.xpath("//h3[contains(text(),'Current time zone:')]"));
                
        return hashMap;
    }
};