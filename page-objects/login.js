module.exports = {

    url: '',

    loginAsAdmin: async function() {
        obj = page.login.elements();
        await helpers.type(obj.get('usernameInput'), shared.login.username, 'false');
        await helpers.type(obj.get('passwordInput'), shared.login.password, 'false');
        await helpers.click(obj.get('submitButton'));
    },

    elements: function() {
        hashMap = new Map();
        hashMap.set('usernameInput', by.xpath('//input[@id=\'id_username\']'));
        hashMap.set('passwordInput', by.xpath('//input[@id=\'id_password\']'));
        hashMap.set('submitButton', by.xpath('//button[@type=\'submit\']'));
        return hashMap;
    }
    
};
