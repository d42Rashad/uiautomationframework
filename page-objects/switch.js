module.exports = {

    url: '',

    elements: function() {
        hashMap = new Map();
        hashMap.set('button_addSNMPAutoDiscoveryJob', by.xpath('//a[contains(text(),\'Add SNMP Autodiscovery Job\')]'));
        hashMap.set('input_jobName', by.xpath('//input[@id=\'id_name\']'));
        hashMap.set('textarea_jobName', by.xpath('//textarea[@id=\'id_server\']'));
        hashMap.set('checkbox_ICMP_TCP_PortCheck', by.xpath('//input[@id=\'id_do_port_check\']'));
        hashMap.set('textbox_CommunityStrings', by.xpath('//input[@id=\'id_enc_snmp_strings\']'));
        hashMap.set('select_SNMPVersion', by.xpath('//select[@id=\'id_snmp_version\']'));
        hashMap.set('div_JobStatus', by.xpath('//div[@id=\'job-status\']'));
        hashMap.set('edit_button', by.xpath('//a[@class=\'historylink\'][text()=\'Edit\']'));
        hashMap.set('addADS_button', by.xpath('//a[contains(text(),\'Add another Auto Discovery Schedule\')]'));
        hashMap.set('label', '//label[text() = \'$\']/following-sibling::p');
        hashMap.set('input', '//label[text() = \'$\']/following-sibling::input');
        hashMap.set('select', '//label[text() = \'$\']/following-sibling::select');
        hashMap.set('sectionShow', '//h2[text()=\'$\']/a[text()=\'Show\']');
        hashMap.set('sectionHide', '//h2[text()=\'$\']/a[text()=\'Hide\']');
        hashMap.set('precedingSpan', '//label[text()=\'$\']');
        hashMap.set('autoDiscoveryColumns', by.xpath('//h2[text() = \'Auto Discovery Schedule\']/following-sibling::table/thead/tr'));
        hashMap.set('autoDiscoveryRows', by.xpath('//h2[text() = \'Auto Discovery Schedule\']/following-sibling::table/tbody/tr[contains(@class,\'dynamic-autoschedule\')]'));
        return hashMap;
    },

    getTableCellElement: async function(searchIndex, clickColumn, columnsElement, rowsElement) {
        tableColumnsBy = await helpers.pages().get('switch').elements().get(columnsElement);
        tableBodyRowBy = await helpers.pages().get('switch').elements().get(rowsElement);
        console.log(`tableColumnsBy: ${tableColumnsBy}`);
        console.log(`tableBodyRowBy: ${tableBodyRowBy}`);
        tableArray = await helpers.getResultTable4(tableColumnsBy, tableBodyRowBy);
        row = await tableArray[parseInt(searchIndex, 10)];
        console.log(`row: ${row}`);
        cell = await row.get(clickColumn);
        childrenBy = by.xpath('child::*');
        cellElements = await cell.findElements(childrenBy);
        console.log(`cellElements.length: ${cellElements.length}`);
        return cellElements;
    },

    getTableRowElement: async function(searchIndex, columnsElement, rowsElement) {
        tableColumnsBy = await helpers.pages().get('switch').elements().get(columnsElement);
        tableBodyRowBy = await helpers.pages().get('switch').elements().get(rowsElement);
        console.log(`tableColumnsBy: ${tableColumnsBy}`);
        console.log(`tableBodyRowBy: ${tableBodyRowBy}`);
        tableArray = await helpers.getResultTable4(tableColumnsBy, tableBodyRowBy);
        row = await tableArray[parseInt(searchIndex, 10)];
        console.log(`row: ${row}`);
        return row;
    }
};
