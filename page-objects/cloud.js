module.exports = {

    url: '',

    addRegions: async function(regions) {
        obj = page.cloud.elements();
        for (i = 0; i < regions.length; i++) {
        	await helpers.clear(obj.get('input_availableRegions'));
        	await helpers.type(obj.get('input_availableRegions'), regions[i], 'false');
        	await helpers.sleep(1);
        	select = await helpers.getElement(obj.get('selectMultiple_availableRegions'));
			option = await select.findElement(by.xpath(`option[text() =\'${regions[i]}\']`));
			await helpers.scrollElementIntoView(option);
			await helpers.highlightElement(option, 'red');
			await option.click();
			await option.click();
			addRegion = await helpers.getElement(obj.get('button_AddRegion'));
			await addRegion.click();
        }        
    },    

    addZones: async function(zones) {
        obj = page.cloud.elements();
        for (i = 0; i < zones.length; i++) {
            await helpers.clear(obj.get('input_availableZones'));
            await helpers.type(obj.get('input_availableZones'), zones[i], 'false');
            await helpers.sleep(1);
            select = await helpers.getElement(obj.get('selectMultiple_availableZones'));
            await helpers.highlightElement(select, 'red');
            console.log(`zones[${i}] = ${zones[i]}`);
            option = await select.findElement(by.xpath(`option[text() =\'${zones[i]}\']`));
            await helpers.scrollElementIntoView(option);
            await helpers.highlightElement(option, 'red');
            await option.click();
            await option.click();
            addRegion = await helpers.getElement(obj.get('button_AddOracleRegion'));
            await addRegion.click();
        }        
    }, 

    elements: function() {
        hashMap = new Map();
        hashMap.set('button_addCloudAutoDiscoveryJob', by.xpath('//a[contains(text(),\'Add Cloud Autodiscovery\')]'));
        hashMap.set('checkbox_kubernetesDiscovery', by.xpath("//label[text()=\'Kubernetes Discovery\']/preceding-sibling::span"));
        hashMap.set('checkbox_kubernetesDiscovery', by.xpath("//input[@id=\'id_kubernetes_discovery\']"));

        hashMap.set('input_availableRegions', by.xpath("//input[@id=\'id_regions_input\']"));
        hashMap.set('input_availableZones', by.xpath("//input[@id=\'id_oracle_regions_input\']"));

        hashMap.set('selectMultiple_availableZones', by.xpath("//select[@name=\'oracle_regions_old\']"));
        hashMap.set('selectMultiple_availableRegions', by.xpath("//select[@id=\'id_regions_from\']"));

        hashMap.set('button_AddRegion', by.xpath("//a[@id=\'id_regions_add_link\']"));
        hashMap.set('button_AddOracleRegion', by.xpath("//a[@id=\'id_oracle_regions_add_link\']"));
        return hashMap;
    }
    
};
