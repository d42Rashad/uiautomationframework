module.exports = {

    url: '',

    elements: function() {
        hashMap = new Map();
        hashMap.set('tableColumns', '//h2[text() = \'$\']/following-sibling::table/thead/tr');
        hashMap.set('tableRows', '//h2[text() = \'$\']/following-sibling::table/tbody/tr');
        return hashMap;
    }
};
