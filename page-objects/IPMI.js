module.exports = {

    url: '',

    elements: function() {
        hashMap = new Map();
        hashMap.set('button_addIPMIRedfishAutoDiscoveryJob', by.xpath('//a[contains(text(),\'Add IPMI/Redfish Autodiscovery Job\')]'));
        hashMap.set('input_jobName', by.xpath('//input[@id=\'id_name\']'));
        hashMap.set('textarea_jobName', by.xpath('//textarea[@id=\'id_server\']'));
        hashMap.set('input_userNamePassword', by.xpath('//input[@name=\'enc_unpwd\']'));
        hashMap.set('textbox_UserPass', by.xpath('//input[@id=\'id_enc_unpwd\']'));
        hashMap.set('select_Discovery_Type', by.xpath('//select[@id=\'id_discovery_type\']'));
        hashMap.set('select_RC', by.xpath('//select[@id=\'id_remote_collector\']'));
        hashMap.set('label', '//label[text() = \'$\']/following-sibling::p');
        hashMap.set('label2', '//label[text() = \'$\']/following-sibling::fieldset');
        hashMap.set('b_text', '//*[@id="dcm_form"]/div/fieldset/div[12]/div/b/text()/following-sibling::a');
        hashMap.set('b_text_button', by.xpath('//a[contains(text(),\'3WLT082\')]'));
        return hashMap;
    }

};
