module.exports = {

    url: '',

    elements: function() {
        hashMap = new Map();
        hashMap.set('networkDiagram', by.xpath('//div[@id=\'networkDiagram\']'));
        hashMap.set('devicesDiagramWrapper', by.xpath('//div[@id=\'devicesDiagramWrapper\']'));
        hashMap.set('networkDiagramWrapper', by.xpath('//div[@id=\'networkDiagramWrapper\']'));
        return hashMap;
    }
};
