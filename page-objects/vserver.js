module.exports = {

    url: '',

    elements: function() {
        hashMap = new Map();
        hashMap.set('button_addHypervisorsAutoDiscoveryJob', by.xpath('//a[contains(text(),\'Add Hypervisors/*nix/win for Autodiscovery\')]'));
        hashMap.set('input_jobName', by.xpath('//input[@id=\'id_name\']'));
        hashMap.set('textarea_jobName', by.xpath('//textarea[@id=\'id_server\']'));
        hashMap.set('input_userNamePassword', by.xpath('//input[@name=\'enc_unpwd\']'));
        hashMap.set('textbox_UserPass', by.xpath('//input[@id=\'id_enc_unpwd\']'));
        hashMap.set('select_Platform', by.xpath('//select[@id=\'id_platform\']'));
        hashMap.set('select_RC', by.xpath('//select[@id=\'id_remote_collector\']'));
        hashMap.set('label', '//label[text() = \'$\']/following-sibling::p');
        return hashMap;
    }

};
