module.exports = {

    url: '',

    elements: function() {
        hashMap = new Map();
        hashMap.set('tableColumns', '//h2[text()=\'$\']/following-sibling::table/thead/tr/th');
        hashMap.set('tableRows', '//h2[text()=\'$\']/following-sibling::table/tbody/tr');
        hashMap.set('tabs', '//ul[@id=\'tabs\']/li/a[text()=\'$\']');
        hashMap.set('ellispses', by.xpath('//i[@class=\'fas fa-ellipsis-h\']'));  
        hashMap.set('dropdownMenuItem', '//li[@id=\'dropdown-tools\']/ul/li/a[contains(text(), \'$\')]')     
        return hashMap;
    }
};
